import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CreateClubPage } from '../create-club/create-club';
import { MyClubPage } from '../my-club/my-club';

/**
 * Generated class for the ClubPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-club',
  templateUrl: 'club.html',
})
export class ClubPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClubPage');
  }

  createPage() {
    this.navCtrl.push(CreateClubPage);
  }
  MyClub() {
    this.navCtrl.push(MyClubPage);
  }

}
