import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AccountPage } from '../pages/account/account';
import { AboutPage } from '../pages/about/about';
import { RecordsPage } from '../pages/records/records';
import { ClubPage } from '../pages/club/club';
import { CreateClubPage } from '../pages/create-club/create-club';
import { MyClubPage } from '../pages/my-club/my-club';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    ClubPage,
    AboutPage,
    MyClubPage,
    RecordsPage,
    CreateClubPage,
    AccountPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    ClubPage,
    MyClubPage,
    AboutPage,
    RecordsPage,
    CreateClubPage,
    AccountPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
